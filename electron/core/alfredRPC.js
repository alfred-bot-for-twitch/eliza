var zerorpc = require('zerorpc');
const { startAlfredApp } = require('../startAlfredBot');

class AlfredRpcServer {
  constructor(tcpUrl, app) {
    this.tcpUrl = tcpUrl;
    this.client = null;
    this.child = null;
    this.app = app;
  }

  start() {
    if (this.client) {
      if (this.client.closed()) {
        this.startServer();
      }
    } else {
      this.startServer();
    }
  }

  startServer() {
    this.child = startAlfredApp(this.app);
    this.client = new zerorpc.Client();
    this.client.connect(this.tcpUrl);
  }

  launchGame(user) {
    this.start();
    this.client.invoke('launch_game', user);
  }

  destroyGame() {
    this.client.close();
    try {
      if(this.child)
        this.child.kill();
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = AlfredRpcServer;
