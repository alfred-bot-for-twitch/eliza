const { BrowserWindow } = require('electron');

function createAppWindow(locationUri, app, rpcServer) {
  // Create the browser window.
  win = new BrowserWindow({
    width: 450,
    height: 650,
    webPreferences: {
      nodeIntegration: true,
    },
    resizable: false,
    minimizable: false,
    maximizable: false,
  });

  // and load the index.html of the app.
  win.loadFile(locationUri);

  // Open the DevTools.
  //win.webContents.openDevTools()

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
    if (rpcServer) rpcServer.destroyGame();
    app.quit();
  });
  return win;
}

module.exports = {
  createAppWindow: createAppWindow,
};
