const spawn = require('child_process').spawn;
const path = require('path');

const startAlfredApp = function(app){
    try {
        const binariesPath = path.join(path.dirname(app.getPath('exe')), '..', './Resources', './bin');
        const execPath = path.resolve(path.join(binariesPath, './alfred'));
        const alfredBot = spawn(execPath, {
            stdio: 'inherit',
            shell: true,
        });
        return alfredBot;
    } catch (error) {
        console.log(error);
        return null;
    }
}


module.exports = {
    startAlfredApp: startAlfredApp
}