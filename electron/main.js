const { app, ipcMain, protocol, BrowserWindow } = require('electron');
const { autoUpdater } = require('electron-updater');
const path = require('path');
const { createAppWindow } = require('./renderer');
const AlfredRpcServer = require('./core/alfredRPC');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win;
let authWindow;
let rpcServer;
let USER_INFO;

const ALFRED_AUTH = 'https://0xggusz0y8.execute-api.eu-central-1.amazonaws.com/latest/';
//const ALFRED_AUTH = 'http://localhost:3000/';
const WEB_HTML = path.join(__dirname, '/public/index.html');
const RPC_ADDRESS = "tcp://0.0.0.0:4242";

protocol.registerSchemesAsPrivileged([
  { scheme: 'https', privileges: { standard: true, secure: true } }
]);

function onAppReady() {
  // try {
  //   autoUpdater.checkForUpdatesAndNotify();
  // } catch (error) {
  //   console.log("cannot update currently");
  // }
  win = createAppWindow(WEB_HTML, app, rpcServer);
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', onAppReady);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    rpcServer.destroyGame();
    app.quit();
  }
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    win = createAppWindow(WEB_HTML, app, rpcServer);
  }
});

ipcMain.on('userTokenAcquired', function(event, args) {
  authWindow.destroy();
  authWindow = null;
  if (!Object.keys(args).every(elem => args[elem] == null)) {
    global.userInfo = args;
    USER_INFO = args;
    win.webContents.send('userInfo', args);
    win.webContents.send('loginSuccess');
  }
});

ipcMain.on('createAuthWindow', function(event, args) {
  authWindow = new BrowserWindow({
    width: 450,
    height: 650,
    show: true,
    webPreferences: {
      nodeIntegration: true,
    },
    resizable: false,
    minimizable: false,
    maximizable: false,
  });
  authWindow.loadURL(`${ALFRED_AUTH}auth/twitch`);
  authWindow.webContents.on('did-finish-load', function(event) {
    authWindow.webContents.executeJavaScript(`
      try{
        const ipcRenderer = require('electron').ipcRenderer;

        let accessToken = localStorage.getItem('accessToken');
        let refreshToken = localStorage.getItem('refreshToken');
        let displayName = localStorage.getItem('display_name');
        let channelId = localStorage.getItem('channel_id');
        let hasuraAccessToken = localStorage.getItem('hasuraAccessToken');
        ipcRenderer.send("userTokenAcquired", {
          accessToken,
          refreshToken,
          displayName,
          channelId,
          hasuraAccessToken
        });
      } catch (err){
        console.log(err);
      }
    `);
  });
  authWindow.on('close', function() {
    authWindow.destroy();
    authWindow = null;
    win = createAppWindow(WEB_HTML, app, rpcServer);
  });
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

ipcMain.on('launchGame', (event, args) => {
  if(!rpcServer)
    rpcServer = new AlfredRpcServer(RPC_ADDRESS, app);
  rpcServer.launchGame(USER_INFO);
  win.webContents.send('rpcConnectionEstablished');
});

ipcMain.on('closeGame', (event, args) => {
  rpcServer.destroyGame();
  win.webContents.send('rpcConnectionClosed');
});

