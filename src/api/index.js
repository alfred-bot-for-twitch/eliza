const ALFRED_AUTH = "http://localhost:3000";

export const get = async path => {
  try {
    const resp = await fetch(`${ALFRED_AUTH}/${path}`);
    let data = await resp.json();
    return data;
  } catch (error) {
      console.log(error);
  }
  return null;
};
